import pandas as pd
import streamlit as st
import plotly.express as px


# https://towardsdatascience.com/streamlit-101-an-in-depth-introduction-fc8aad9492f2

@st.cache # downloaded once and CACHED for future usage.
def get_data():
    url = "http://data.insideairbnb.com/united-states/ny/new-york-city/2019-09-12/visualisations/listings.csv"
    return pd.read_csv(url)

df = get_data()

# Generate Main Title
st.title("This is a Streamlit 101 Title!")

# Generate GitHub Markdown
st.markdown("THis is a markdown to something")

# Generate Header
st.header("Customary Quote")
st.markdown("I just wnat to go home... ")

# Render Dataframe
st.dataframe(df.head())

# Render code block
st.code("""
@st.cache
def get_data():
    url = "http://data.insideairbnb.com/[...]"
    return pd.read_csv(url)
""", language='python')

# Render Echo:
with st.echo():
    st.dataframe(df.head())

# Render Map:
st.subheader("NYC AirBNB cheaper than 50")
st.map(df.query("price<=50")[["latitude", "longitude"]].dropna(how="any"))


st.subheader("NYC AirBNB greater than 800")
st.map(df.query("price>=800")[["latitude", "longitude"]].dropna(how="any"))

st.subheader("NYC AirBNB greater than 5600")
st.map(df.query("price>=5600")[["latitude", "longitude"]].dropna(how="any"))

st.subheader("Quickly MultiSelect")
cols = ["name", "host_name", "neighbourhood", "room_type", "price"]
st_multiselect = st.multiselect("Columns", df.columns.tolist(), default=cols)
st.dataframe(df[st_multiselect].head(10))

st.header("Static Table: Average Price by Room Type")
st.table(
    df.groupby("room_type").price.mean().reset_index().round(2).sort_values("price", ascending=False).assign(avg_price=lambda x : x.pop("price").apply(lambda y: "%.2f" % y))
)

st.header("JSON interactive Rendering")
listingcounts = df.host_id.value_counts()
top_host_1 = df.query("host_id==@listingcounts.index[0]")
top_host_2 = df.query("host_id==@listingcounts.index[1]")
st.json({
    top_host_1.iloc[0].host_name: top_host_1[["name", "neighbourhood", "room_type", "minimum_nights", "price"]].sample(2, random_state=4).to_dict(orient="records"),
    top_host_2.iloc[0].host_name: top_host_1[["name", "neighbourhood", "room_type", "minimum_nights", "price"]].sample(2, random_state=4).to_dict(orient="records"),
})

st.header("Interactive Plotly integration")
values = st.sidebar.slider("Price range", float(df.price.min()), 500., (50., 300.))
f = px.histogram(df.query(f"price.between{values}"), x="price", nbins=15, title="Price distribution")
f.update_xaxes(title="Price")
f.update_yaxes(title="No. of Listings")
st.plotly_chart(f)

st.header("Using Radio Button to Restrict Selection")
neighborhood = st.radio("Neighborhood", df.neighbourhood_group.unique())
show_exp = st.checkbox("Include expensive listings")
show_exp = " and price<200" if not show_exp else ""

@st.cache
def get_availability(show_exp, neighborhood):
    return df.query(f"""neighbourhood_group==@neighborhood{show_exp} and availability_365>0""").availability_365.describe(percentiles=[.1, .25, .5, .75, .9, .99]).to_frame().T

st.table(get_availability(show_exp, neighborhood))

# plot = df.query("availability_365>0") \
#     .groupby("neighbourhood_group") \
#     .availability_365.mean() \
#     .plot.bar(rot=0)
# plot.set(title="Average availability by neighborhood group", xlabel="Neighborhood group", ylabel="Avg. availability (in no. of days)")
# st.pyplot(fig=plot)

st.header("Use Control to Restrict Selection")

minimum = st.sidebar.number_input("Minimum", min_value=0.0, max_value=486.0)
maximum = st.sidebar.number_input("Maximum", min_value=0.0, max_value=486.0, value=5.0)
if minimum > maximum:
    st.error("Please enter a valid range")
else:
    df.query("@minimum<=number_of_reviews<=@maximum").sort_values("number_of_reviews", ascending=False).head(50)[["name", "number_of_reviews", "neighbourhood", "host_name", "room_type", "price"]]


st.header("Images:")
pics = {
    "Cat": "https://cdn.pixabay.com/photo/2016/09/24/22/20/cat-1692702_960_720.jpg",
    "Puppy": "https://cdn.pixabay.com/photo/2019/03/15/19/19/puppy-4057786_960_720.jpg",
    "Sci-fi city": "https://storage.needpix.com/rsynced_images/science-fiction-2971848_1280.jpg"
}
pic = st.selectbox("Picture choices", list(pics.keys()), 0)
st.image(pics[pic], use_column_width=True, caption=pics[pic])